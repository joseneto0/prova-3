#!/bin/bash

vidaJogador=$(shuf -i 10-200 -n 1)
vidaDragao=$(shuf -i 100-5000 -n 1)

while true; do
	if [ $vidaJogador -le 0 ]; then
		echo "Jhonsons derrotou zezin e fez um banquete com seus restos mortais :O"
		break;
	fi
	if [ $vidaDragao -le 0 ]; then
		echo "Zezin bravamente, derrotou o dragão :D"
		break;
	fi
	echo "1 -> Atacar;"
	echo "2 -> Fugir;"
	echo "3 -> Cura;"
	echo "4 -> Info;"
	read -p "Escolha uma alternativa: " x
	if [ $x -eq 1 ]; then
		dano=$(shuf -i 10-100 -n 1)
		vidaDragao=$((vidaDragao-dano))
		qtdDrag=$(shuf -i 1-5 -n 1)
		for numero in $(seq 1 $qtdDrag); do
			danoDragao=$(shuf -i 1-10 -n 1)
			vidaJogador=$((vidaJogador - danoDragao))
		done
	elif [ $x -eq 2 ]; then
		chance=$(shuf -i 1-10 -n 1)
		if [ $chance -lt 3 ]; then
			echo "Zezin conseguiu fugir :)"
			break
		else
			echo "Você morreu para a baforada do JHonsons :c"
			break
		fi
	elif [ $x -eq 3 ]; then
		cura=$(shuf -i 50-100 -n 1)
		vidaJogador=$((vidaJogador + cura))
		chanceFuga=$(shuf -i 1-10 -n 1)
		if [ $chanceFuga -eq 1 ]; then
			echo "Jhonsons fugiu enquanto você se curava :c"
			break;
		else
			echo "Zezin se curou em $cura"
		fi
	elif [ $x -eq 4 ]; then
		echo "Vida do Zezin: $vidaJogador"
		echo "Vida do Jhonsons: $vidaDragao"
	fi
done
