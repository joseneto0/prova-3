#!/bin/bash

function op1() {
	id $1 &> /dev/null
	if [ $? -eq 0 ]
	then
		echo "O usuário $1 existe :D"
	else
		echo "O usuário $1 não existe :C"
	fi
}

function op2(){
	usuarioAtual=$(whoami)
	if [ "$1" = "$usuarioAtual" ] 
	then
		echo "O usuário $1 está logado atualmente no sistema"
		return 0
	else
		echo "O usuário $1 não está logado no sistema"
		return 1
	fi
}

function op3() {
	ls /home/$1 &> /dev/null
	if [ $? -eq 0 ]
	then
		echo "Arquivos da pasta home do usuário $1: "
		ls /home/$1
		return 0
	else
		echo "O usuário não existe :c, não vai dar"
		return 1
	fi
}



