#!/bin/bash
source functions.sh
while true; do
	echo "1 -> Verifica se o usuário existe;"
	echo "2 -> Verifica se o usuário está logado na máquina;"
	echo "3 -> Lista os arquivos da pasta home do usuário;"
	echo "4 -> Sair;"
	read -p "Escolha a opção: " x
	case $x in
		1) op1 $1;;
		2) op2 $1; if [ $? -eq 1 ]; then break; fi;;
		3) op3 $1; if [ $? -eq 1 ]; then break; fi;;
		4) echo "Obrigado por utilizar o programa :)"; break;;	
	esac
done
