#!/bin/bash

vidaJogador=$1
vidaDragao=$2

while true; do
	if [ $vidaJogador -le 0 ]; then
		echo "O dragão derrotou o jogador e fez um banquete com seus restos mortais :O"
		break;
	fi
	if [ $vidaDragao -le 0 ]; then
		echo "O jogador bravamente, derrotou o dragão :D"
		break;
	fi
	echo "1 -> Atacar;"
	echo "2 -> Fugir;"
	echo "3 -> Informações;"
	read -p "Escolha uma alternativa: " x
	case $x in
		1) echo "Você causou 100 de dano e recebeu 10 de dano"; vidaDragao=$((vidaDragao - 100)); vidaJogador=$((vidaJogador - 10));;
		2) if [ $(shuf -i 1-10 -n 1) -gt 5 ]; then echo "Você conseguiu escapar :D"; break; else echo "Você morreu para a baforada do dragão enquanto fugia :c"; break; fi;; 
		3) echo "Informações uteis: "; echo "Vida do Jogador: $vidaJogador"; echo "Vida do Dragão: $vidaDragao";;
	esac
done
